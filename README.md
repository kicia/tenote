# A simple GUI in ubuntu

## Install

See also: https://thescienceofcode.com/imgui-quickstart/

Install GLFW Development library
```
apt-get install libglfw-dev
```

(?) Execute `make` while in the project root. Inside `bin/` we will find the compiled executable

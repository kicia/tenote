# =====================================================================
# Makefile (Linux)
#
# You will need GLFW (http://www.glfw.org):
# apt-get install libglfw-dev
# =====================================================================

BIN_FOLDER = bin
EXE = bin/imgui-test
SOURCES = main.cpp
SOURCES += imgui/imgui.cpp imgui/imgui_demo.cpp imgui/imgui_draw.cpp imgui/imgui_tables.cpp imgui/imgui_widgets.cpp
SOURCES += imgui/backends/imgui_impl_glfw.cpp imgui/backends/imgui_impl_opengl3.cpp
OBJS = $(addsuffix .o, $(basename $(notdir $(SOURCES))))
LINUX_GL_LIBS = -lGL

CXXFLAGS = -std=c++11 -Iimgui -Iimgui/backends -Isrc
CXXFLAGS += -g -Wall -Wformat
LIBS =

##---------------------------------------------------------------------
## OPENGL ES
##---------------------------------------------------------------------

## This assumes a GL ES library available in the system, e.g. libGLESv2.so
# CXXFLAGS += -DIMGUI_IMPL_OPENGL_ES2
# LINUX_GL_LIBS = -lGLESv2

##---------------------------------------------------------------------
## BUILD FLAGS
##---------------------------------------------------------------------

LIBS += $(LINUX_GL_LIBS) `pkg-config --static --libs glfw3`
CXXFLAGS += `pkg-config --cflags glfw3`
CFLAGS = $(CXXFLAGS)

##---------------------------------------------------------------------
## BUILD RULES
##---------------------------------------------------------------------

%.o:%.cpp
	g++ $(CXXFLAGS) -c -o $@ $<

%.o:imgui/%.cpp
	g++ $(CXXFLAGS) -c -o $@ $<

%.o:imgui/backends/%.cpp
	g++ $(CXXFLAGS) -c -o $@ $<

all: SHELL:=/bin/bash
all: $(EXE)
	@echo Build complete for Linux

$(EXE): $(OBJS)
	if [ ! -d $(BIN_FOLDER) ]; then \
		mkdir $(BIN_FOLDER);        \
	fi
	rm -f bin/*
	g++ -o $@ $^ $(CXXFLAGS) $(LIBS)

clean:
	rm -f $(EXE) $(OBJS)
